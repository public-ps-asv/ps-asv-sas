import {Result} from "../../Entities/Result";

export abstract class AbstractDocumentVerification {
  abstract VerifySignatures(
    firstSignatureImage: string,
    secondSignatureImage: string
  ): Promise<Result<number>>;
}
