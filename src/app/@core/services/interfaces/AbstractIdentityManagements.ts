import {Result} from "../../Entities/Result";

export interface IdentityInfo {
  AccessToken: string;
  RefreshToken: string;
  ExpiresAt: string;
  RefreshExpiresAt: string;
}


export abstract class AbstractIdentityManagements {
  abstract LogIn(
    username: string,
    password: string,
  ): Promise<Result<IdentityInfo>>;

  abstract RefreshAccessToken(
    refreshToken: string
  ): Promise<Result<IdentityInfo>>;

  abstract Logout(refreshToken:string): Promise<Result<boolean>>;
}
