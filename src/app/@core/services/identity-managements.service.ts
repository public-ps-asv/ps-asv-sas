import {Injectable} from '@angular/core';
import {AbstractIdentityManagements, IdentityInfo} from "./interfaces/AbstractIdentityManagements";
import {Result} from "../Entities/Result";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {EnvService} from "./env.service";
import {DatePipe} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class IdentityManagementsService extends AbstractIdentityManagements {

  API_URL = "";

  constructor(private http: HttpClient, private envService: EnvService,private datepipe: DatePipe) {
    super();
    this.API_URL = `${envService.identityServer.URL}/auth/realms/${envService.identityServer.Realm}/protocol/openid-connect/`;
  }

  async LogIn(username: string, password: string): Promise<Result<IdentityInfo>> {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/x-www-form-urlencoded"
        }),
      };
      const params = new HttpParams({
        fromObject: {
          grant_type: 'password',
          client_id: this.envService.identityServer.clientId,
          username: username,
          password: password
        }
      });

      const result: any = await this.http
        .post(
          `${this.API_URL}token`,
          params,
          httpOptions
        )
        .toPromise();
      let currentDate = new Date();
      currentDate.setTime(currentDate.getTime() + result.expires_in * 1000);
      let currentDateTime =this.datepipe.transform(currentDate, 'MM/dd/yyyy HH:mm:ss');
      currentDate = new Date();
      currentDate.setTime(currentDate.getTime() + result.refresh_expires_in * 1000);
      let refreshTokenExpiresAt =this.datepipe.transform(currentDate, 'MM/dd/yyyy HH:mm:ss');
      return Result.Success({
        AccessToken: result.access_token,
        RefreshToken: result.refresh_token,
        ExpiresAt:  currentDateTime,
        RefreshExpiresAt: refreshTokenExpiresAt
      })
    } catch (error) {
      if (error.status == 403 || error.status == 401)
        return Result.Fail({...error, error: "User not authorized"});
      return Result.Fail(error);
    }
  }

  async RefreshAccessToken(refreshToken: string): Promise<Result<IdentityInfo>> {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/x-www-form-urlencoded"
        }),
      };
      const params = new HttpParams({
        fromObject: {
          grant_type: 'refresh_token',
          client_id: this.envService.identityServer.clientId,
          refresh_token: refreshToken
        }
      });
      const result: any = await this.http
        .post(
          `${this.API_URL}token`,
          params,
          httpOptions
        )
        .toPromise();
      let currentDate = new Date();
      currentDate.setTime(currentDate.getTime() + result.expires_in * 1000);
      let currentDateTime =this.datepipe.transform(currentDate, 'MM/dd/yyyy HH:mm:ss');
      currentDate = new Date();
      currentDate.setTime(currentDate.getTime() + result.refresh_expires_in * 1000);
      let refreshTokenExpiresAt =this.datepipe.transform(currentDate, 'MM/dd/yyyy HH:mm:ss');

      return Result.Success({
        AccessToken: result.access_token,
        RefreshToken: result.refresh_token,
        ExpiresAt:  currentDateTime,
        RefreshExpiresAt: refreshTokenExpiresAt
      })
    } catch (error) {
      if (error.status == 403 || error.status == 401)
        return Result.Fail({...error, error: "User not authorized"});
      return Result.Fail(error);
    }
  }

  async Logout(refreshToken:string): Promise<Result<boolean>> {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/x-www-form-urlencoded"
        }),
      };
      const params = new HttpParams({
        fromObject: {
          client_id: this.envService.identityServer.clientId,
          refresh_token: refreshToken
        }
      });

      const result: any = await this.http
        .post(
          `${this.API_URL}logout`,
          params,
          httpOptions
        )
        .toPromise();
      return Result.Success(true);
    } catch (error) {
      if (error.status == 403 || error.status == 401)
        return Result.Fail({...error, error: "User not authorized"});
      return Result.Fail(error);
    }
  }
}
