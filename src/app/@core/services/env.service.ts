import {Injectable} from "@angular/core";

@Injectable()
export class EnvService {
  public gateway = "http://127.0.0.1:5010/asv/api/";
  public identityServer = {
    URL: "https://keycloak.machine-dev.progressoft.cloud",
    Realm: "ASV",
    clientId: "ASV-SAAS",
  };
  public codeRepo = "https://gitlab.com/public-ps-asv/ps-asv-sas";

  constructor() {
  }
}
