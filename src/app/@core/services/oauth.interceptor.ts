import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {AbstractIdentityManagements} from "./interfaces/AbstractIdentityManagements";
import {fromPromise} from "rxjs/internal-compatibility";
import {switchMap} from "rxjs/operators";

@Injectable()
export class OAuthInterceptor implements HttpInterceptor {

  constructor(private identityService:AbstractIdentityManagements) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(!request.url.includes("VerifySignatures"))
      return next.handle(request);
    const identityInfoString = localStorage.getItem('identityInfo');
    if(identityInfoString == null)
      return next.handle(request);

    const identityInfo =JSON.parse(identityInfoString);
    const accessTokenExpiresAt = new Date(identityInfo.ExpiresAt);
    const refreshTokenExpiresAt = new Date(identityInfo.RefreshExpiresAt);
    const currentDateTime = new Date().getTime();
    if(accessTokenExpiresAt.getTime() < currentDateTime && refreshTokenExpiresAt.getTime() > currentDateTime ){
      return fromPromise(this.identityService.RefreshAccessToken(identityInfo.RefreshToken)).pipe(switchMap(item => {
        localStorage.setItem('identityInfo',JSON.stringify(item.value));
        return next.handle(request.clone({
          setHeaders: {
            "Authorization": `Bearer ${item.value.AccessToken}`
          }
        }));
      }));
    }

    return next.handle(request.clone({
      setHeaders: {
        "Authorization": `Bearer ${identityInfo.AccessToken}`
      }
    }));
  }
}
