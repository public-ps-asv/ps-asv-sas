import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AbstractDocumentVerification} from "./interfaces/AbstractDocumentVerification";
import {Result} from "../Entities/Result";
import {EnvService} from "./env.service";

@Injectable({
  providedIn: 'root'
})
export class DocumentVerificationService extends AbstractDocumentVerification {
  API_URL = "";

  constructor(private http: HttpClient, private envService: EnvService) {
    super();
    this.API_URL = `${envService.gateway}DocumentVerification/`;
  }

  async VerifySignatures(
    firstSignatureImage: string,
    secondSignatureImage: string
  ): Promise<Result<number>> {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        }),
      };
      const result: any = await this.http
        .post(
          `${this.API_URL}VerifySignatures`,
          {firstSignatureImage, secondSignatureImage },
          httpOptions
        )
        .toPromise();
      return Result.Success<number>(result.similarityScore);
    } catch (error) {
      if (error.status == 403 || error.status == 401)
        return Result.Fail({...error, error: "User not authorized"});
      return Result.Fail<number>(error);
    }
  }
}
