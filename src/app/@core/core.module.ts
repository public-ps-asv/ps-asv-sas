import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { AbstractDocumentVerification } from './services/interfaces/AbstractDocumentVerification'
import {EnvService} from './services/env.service'
import { DocumentVerificationService } from './services/document-verification.service'
import { EnvServiceProvider } from './services/env.service.provider'
import {AbstractIdentityManagements} from "./services/interfaces/AbstractIdentityManagements";
import {IdentityManagementsService} from "./services/identity-managements.service";

const DATA_SERVICES = [
  {provide: AbstractDocumentVerification, useClass: DocumentVerificationService},
  {provide: AbstractIdentityManagements, useClass: IdentityManagementsService}
];

export const NB_CORE_PROVIDERS = [
  ...DATA_SERVICES];

@NgModule({
  imports: [
    CommonModule
  ]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [...NB_CORE_PROVIDERS]
    };
  }
}
