export class Result<Type> {
  value: Type;
  IsSuccess: boolean;
  error: any;

  constructor(value: Type, isSuccess: boolean, error: any) {
    this.IsSuccess = isSuccess;
    if (value !== null) this.value = value;
    if (error !== null) this.error = error;
  }

  static Success<Type>(value: Type): Result<Type> {
    return new Result<Type>(value, true, null);
  }

  static Fail<Type>(error: any): Result<Type> {
    return new Result<Type>(null, false, error);
  }
}
