import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {EnvServiceProvider} from "./@core/services/env.service.provider";
import {CoreModule} from "./@core/core.module";
import {AppComponent} from "./app.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ThemeModule} from "./@theme/@theme.module";
import {DatePipe} from "@angular/common";
import {OAuthInterceptor} from "./@core/services/oauth.interceptor";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    MatToolbarModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
  ],
  providers: [
    EnvServiceProvider,
    DatePipe,
    {
    provide: HTTP_INTERCEPTORS,
    useClass: OAuthInterceptor,
    multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
