import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlockLoadingModelComponent } from "./components/block-loading-model/block-loading-model.component";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { VerificationResultComponent } from './components/verification-result/verification-result.component';
import {MatCardModule} from "@angular/material/card";
import { LoginComponent } from './components/login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [BlockLoadingModelComponent, VerificationResultComponent, LoginComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule
  ]
  ,exports:[BlockLoadingModelComponent]
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders<ThemeModule> {
    return {
      ngModule: ThemeModule,
    };
  }
}
