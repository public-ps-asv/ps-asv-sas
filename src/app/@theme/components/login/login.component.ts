import {Component, OnInit} from '@angular/core';
import {AbstractIdentityManagements} from "../../../@core/services/interfaces/AbstractIdentityManagements";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginValid = true;
  public loginErrorMessage = "";
  public username = '';
  public password = '';

  constructor(public dialogRef: MatDialogRef<LoginComponent>, private identityManagements: AbstractIdentityManagements) {
  }

  public ngOnInit(): void {

  }

  public async onSubmit() {
    const result = await this.identityManagements.LogIn(this.username, this.password);
    if (!result.IsSuccess) {
      this.loginErrorMessage = result.error.error;
      this.loginValid = false;
      return;
    }
    localStorage.setItem('identityInfo', JSON.stringify(result.value));
    this.dialogRef.close();
  }
}
