import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

export interface DialogData {
  verificationSucceeded: boolean;
  verificationErrorMessage: string;
  verificationValue:string;
}

@Component({
  selector: 'app-verification-result',
  templateUrl: './verification-result.component.html',
  styleUrls: ['./verification-result.component.css']
})
export class VerificationResultComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<VerificationResultComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              ) {

  }

  ngOnInit(): void {


  }

}
