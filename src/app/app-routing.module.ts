import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: "asv",
    loadChildren: () =>
      import("./pages/pages.module").then((m) => m.PagesModule)
  },
  {path: "", redirectTo: "asv", pathMatch: "full"},
  {path: "**", redirectTo: "asv"}];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
