import {Component, ElementRef, ViewChild} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {BlockLoadingModelComponent} from "../../@theme/components/block-loading-model/block-loading-model.component";
import {Result} from "../../@core/Entities/Result";
import {VerificationResultComponent} from "../../@theme/components/verification-result/verification-result.component";
import {AbstractDocumentVerification} from "../../@core/services/interfaces/AbstractDocumentVerification";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './signatureToSignature.component.html',
  styleUrls: ['./signatureToSignature.component.css']
})
export class SignatureToSignatureComponent {
  @ViewChild('signatureOne', {static: true})
  signatureOne: ElementRef<HTMLDivElement>;

  @ViewChild('signatureTwo', {static: true})
  signatureTwo: ElementRef<HTMLDivElement>;

  jwt = '';

  constructor(private documentVerificationService: AbstractDocumentVerification,
              public dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute) {
  }

  signatureSelected(event: any, signatureId: any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      signatureId.style.backgroundImage = 'url(' + reader.result + ')';
      signatureId.style.backgroundSize = 'contain';
    };
  }

  async VerifySignatures() {
    if (this.signatureOne.nativeElement.style.backgroundImage == '' || this.signatureTwo.nativeElement.style.backgroundImage == '')
      return

    const sigImageOne = this.signatureOne.nativeElement.style.backgroundImage.replace('url("', '')
      .replace('")', '').split(',')[1];

    const sigImageTwo = this.signatureTwo.nativeElement.style.backgroundImage.replace('url("', '')
      .replace('")', '').split(',')[1];

    const anotherResult = await this.loadBlockSection(async () => {
      return await this.documentVerificationService.VerifySignatures(sigImageOne, sigImageTwo);
    });
    this.dialog.open(VerificationResultComponent, {
      data: {
        verificationErrorMessage: !anotherResult.IsSuccess ? typeof anotherResult.error.error === 'object' ? anotherResult.error.error.ErrorMessage : anotherResult.error.error.split('.')[0] : "",
        verificationSucceeded: anotherResult.IsSuccess,
        verificationValue: anotherResult.value
      }
    });
  }

  async loadBlockSection(callbackContext: () => Promise<Result<any>>): Promise<Result<any>> {
    const dialogRef = this.dialog.open(BlockLoadingModelComponent, {disableClose: true});
    const result = await callbackContext();
    dialogRef.close();
    return result;
  }

  navigateToDocumentationView() {
    this.router.navigate(['documentation'], {relativeTo: this.route});
  }


}
