import { NgModule } from "@angular/core";

import { PagesComponent } from "./pages.component";
import { PagesRoutingModule } from "./pages-routing.module";
import { SignatureToSignatureComponent } from './trials/signatureToSignature.component'
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ThemeModule} from "../@theme/@theme.module";
import {MatDialogModule} from "@angular/material/dialog";
import {CommonModule} from "@angular/common";
import { ApiDocumentationComponent } from './api-documentation/api-documentation.component';

@NgModule({
  imports: [
    PagesRoutingModule,
    MatToolbarModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    ThemeModule,
    CommonModule,
  ],
  declarations: [PagesComponent,SignatureToSignatureComponent, ApiDocumentationComponent],
})
export class PagesModule {}
