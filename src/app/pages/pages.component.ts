import {Component, OnInit} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {LoginComponent} from "../@theme/components/login/login.component";
import {AbstractIdentityManagements} from "../@core/services/interfaces/AbstractIdentityManagements";
import {Router} from "@angular/router";

@Component({
  selector: "ngx-pages",
  template: `
    <mat-toolbar >
      <span style="color: navy">PS-ASV</span>
      <span style="flex: 1 1 auto;"></span>
      <button *ngIf=" isAlreadyLoggedIn()" mat-button style="background-color: navy;color:white"
              (click)="loginClicked()">
        Login
      </button>
      <button *ngIf="!isAlreadyLoggedIn()" mat-button style="background-color: navy;color:white"
              (click)="logoutUser()">
        Logout
      </button>
    </mat-toolbar>
    <router-outlet></router-outlet>
  `
})
export class PagesComponent implements OnInit{

  constructor(public dialog: MatDialog,private identityService: AbstractIdentityManagements,private _router: Router) {
  }

  ngOnInit(): void {
  }

  loginClicked() {
    this.dialog.open(LoginComponent);
  }

  isAlreadyLoggedIn() {
    const identityInfo = localStorage.getItem('identityInfo');
    if(identityInfo == undefined || identityInfo == 'undefined')
    {
      localStorage.removeItem('identityInfo');
      return true;
    }
    if(identityInfo == null)
      return true;
    const refreshExpiresAtDate = new Date(JSON.parse(identityInfo).RefreshExpiresAt);
    if(refreshExpiresAtDate.getTime() < new Date().getTime()) {
      localStorage.removeItem('identityInfo');
      return true;
    }
    return false;
    }

  async logoutUser() {
    const identityInfo = localStorage.getItem('identityInfo');
    if(identityInfo == undefined || identityInfo == 'undefined'){
      localStorage.removeItem('identityInfo');
      return;
    }
    if(identityInfo == null)
      return;
    await this.identityService.Logout(JSON.parse(identityInfo).RefreshToken);
    localStorage.removeItem('identityInfo');
  }
}
