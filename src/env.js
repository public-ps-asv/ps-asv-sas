(function (window) {
  window.__env = window.__env || {};
  window.__env.gateway = "https://singapore-stg-asvapp-asv-core.pslabs.sh/asv/api/";
  window.__env.codeRepo = "https://gitlab.com/public-ps-asv/ps-asv-sas";
  window.__env.identityServer = {};
  window.__env.identityServer.URL = "https://singapore-stg-pssec.pslabs.sh";
  window.__env.identityServer.Realm = "ASV";
  window.__env.identityServer.clientId = "ASV-SAAS"
}(this));
