# Progressoft

### Signature to Signature Verification
**Request URL**
    https://{core-api-domain}/asv/api/DocumentVerification/VerifySignatures
    
**Request Type**
    Post

**Request Header**
| Entity | Type | Description | Required|
| ------ | ------ | ------ | ------ |
| Authorization | String | Valid bearer authentication token provided from the login API  | Yes |
| Content-Type | String |  specifing request body content type and should be ``` application/json ``` | Yes |

**Request Body**
| Entity | Type | Description | Required|
| ------ | ------ | ------ | ------ |
| firstSignatureImage | string | signature image base64 encoded | Yes |
| secondSignatureImage | string | signature image base64 encoded | Yes |

**Sample Request**

```sh
{
    firstSignatureImage: "Base64 Encoded Image",
    secondSignatureImage: "Base64 Encoded Image"
}
```
**Response Body (Status code 200)**
| Entity | Type | Description |
| ------ | ------ | ------ | 
| similarityScore | Number | the similarity score between the two send signatures |

**Sample Response (Status code 200)**
```sh
{
    "similarityScore": 88
}
```

**Response Body (Status code not 200)**
| Entity | Type | Description |
| ------ | ------ | ------ | 
| errorMessage | string | Describe the failure in receiving/processing the request |

**Sample Response (Status code not 200)**
```sh
{
    "errorMessage": "User not Authorized"
}
```


### Login API Using username & password
**Request URL**
    https://{security-domain}/auth/realms/ASV/protocol/openid-connect/token

**Request Type**
    Post

**Request Header**
| Entity | Type | Description | Required|
| ------ | ------ | ------ | ------ |
| Content-Type | String |  specifing request body content type and should be ``` application/x-www-form-urlencoded ``` | Yes |

**Request Body**
| Entity | Type | Description | Required|
| ------ | ------ | ------ | ------ |
| client_id | string | specify for which clinet to connect value should be ``` ASV-SAAS ```| Yes |
| grant_type | string | specify how to user will login value should be ``` password ```| Yes |
| username | string | username provided to the client | Yes |
| password | string | password provided to the client | Yes |

**Sample Request**

```sh
application/x-www-form-urlencoded data:
{
    "client_id": "ASV-SAAS",
    "grant_type": "password",
    "username": "client-username",
    "password": "***************"
  }
```
**Response Body**
| Entity | Type | Description |
| ------ | ------ | ------ | 
| access_token | String | Authentication token used to access ASV API's  |
| expires_in | Number | token lifespan in seconds |
| refresh_token | String | authentication token used to regenerate a new access token without using username and password  |
| refresh_expires_in | Number | Refresh token lifespan in seconds |
| token_type | String | type of the Authentication token value will be ```bearer``` |

**Sample Response**
```sh
{
    "access_token": "encoded string",
    "expires_in": 300,
    "refresh_token" : "encoded string",
    "refresh_expires_in": 1800,
    "token_type" : "bearer"
}
```


### Login API Using Refresh token
**Request URL**
    https://{security-domain}/auth/realms/ASV/protocol/openid-connect/token
    
**Request Type**
    Post

**Request Header**
| Entity | Type | Description | Required|
| ------ | ------ | ------ | ------ |
| Content-Type | String |  specifing request body content type and should be ``` application/x-www-form-urlencoded ``` | Yes |

**Request Body**
| Entity | Type | Description | Required|
| ------ | ------ | ------ | ------ |
| client_id | string | specify for which clinet to connect value should be ``` ASV-SAAS ```| Yes |
| grant_type | string | specify how to user will login value should be ``` refresh_token ```| Yes |
| refresh_token | string | encoded string provided from the login api | Yes |

**Sample Request**

```sh
application/x-www-form-urlencoded data:
{
    "client_id": "ASV-SAAS",
    "grant_type": "refresh_token",
    "refresh_token": "encoded string"
  }
```
**Response Body**
| Entity | Type | Description |
| ------ | ------ | ------ | 
| access_token | String | Authentication token used to access ASV API's  |
| expires_in | Number | token lifespan in seconds |
| refresh_token | String | authentication token used to regenerate a new access token without using username and password  |
| refresh_expires_in | Number | Refresh token lifespan in seconds |
| token_type | String | type of the Authentication token value will be ```bearer``` |

**Sample Response**
```sh
{
    "access_token": "encoded string",
    "expires_in": 300,
    "refresh_token" : "encoded string",
    "refresh_expires_in": 1800,
    "token_type" : "bearer"
}
```
